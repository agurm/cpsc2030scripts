#!/bin/bash
filename=xampp-linux-x64-7.3.8-2-installer.run
rm $filename > /dev/null
#download
wget https://www.apachefriends.org/xampp-files/7.3.8/$filename 
chmod +x $filename

#install
./$filename << OPTION
Y
Y
Y

Y

OPTION
#change config to 8080
sed "s/^Listen 80$/Listen 8080/" /opt/lampp/etc/httpd.conf  >new.conf
cp new.conf /opt/lampp/etc/httpd.conf

#setup directories 
mkdir /opt/lampp/htdocs/cpsc2030
chmod +rwx /opt/lampp/htdocs/cpsc2030
ln -s /opt/lampp/htdocs/cpsc2030 www
chown ec2-user:ec2-user /opt/lampp/htdocs/cpsc2030
cat > www/index.html << FILE
Test string for index page
FILE
chown ec2-user:ec2-user /opt/lampp/htdocs/cpsc2030/index.html

#start services
/opt/lampp/xampp start
#incase it's already started
/opt/lampp/xampp restart
#clean up
rm $filename
rm new.conf
